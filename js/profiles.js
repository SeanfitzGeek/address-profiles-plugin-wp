jQuery(function ($) {
    var address_type = 0;

    $('body').on('click', '.profiles-save-billing-js, .profiles-save-shipping-js', function (event) {
        var fields = {
            name: null,
            fields: null
        };
        event.preventDefault();

        $.ajax({
            url: ap_ajax_url,
            data: {
                action: 'ap_update_profile',
                user: $(this).parent().data('user'),
                security: $(this).parent().data('security'),
                address_type: $(this).parent().data('address-type'),
                fields: fields,
                // profile_id: $(this).parent().parents().find('').data('profile-id')
            },
            type: 'post',
            success: function (reply) {
                // reply = JSON.parse(reply);
                console.log(reply);

                $('#ap-address-profile-select').load(document.URL + ' #ap-address-profile-select');

            }
        });
    });

    $('body').on('click', '.js-add-profile', function () {
        var profileName = $(this).parent().parent().find('input:first');

        if (profileName.val().length === 0) {
            alert('Profile Name must not be empty!');

            return;
        }

        $.ajax({
            url: ap_ajax_url,
            data: {
                action: 'ap_update_profile',
                user: $(this).data('user'),
                security: $(this).data('security'),
                address_type: address_type,
                profile_name: profileName.val(),
                fields: {},
                type: 'add',
            },
            type: 'post',
            success: function (reply) {
                // reply = JSON.parse(reply);

                var address = $('.ap-address-load-fix');

                address.load(document.URL + ' .ap-address-load-fix', null, function () {
                    address.find('select option[value=' + parseInt(reply) + ']').attr('selected', 'selected');

                    loadAddressData();
                });

                $('#address-profile-modal').modal('hide');

            }
        });
    });

    $('#new-billing-profile').click(function () {
        address_type = 0;
    });

    $('#new-shipping-profile').click(function () {
        address_type = 1;
    });

    function loadAddressData() {

    }
});