<?php
namespace Model;

class AddressProfilesModal {

    const TABLE_NAME = 'Address_Profiles';

    public function insertAddressProfile() {
        global $wpdb;

        $wpdb->insert(
            self::TABLE_NAME,
            [
                'user' => (int)$_POST['user'],
                'profile_name' => $_POST['profile_name'],
                'profile_type' => (int)$_POST['address_type'],
                'profile_fields' => '',
            ]
        );
    }

    public function getProfilesByType($type, $user) {
        global $wpdb;

        $addressProfiles = $wpdb->prepare("
            SELECT a.*
            FROM address_profiles a
            WHERE a.user = '%d'
            AND a.profile_type = '%s'
            ORDER BY a.profile_name ASC
            ", $user, $type);

        return $wpdb->get_results($addressProfiles, OBJECT);
    }

    public function updateProfile() {
        global $wpdb;

        if (($user = $_POST['user']) === 0)
            die('Invalid Security Check');

        if (!wp_verify_nonce($_POST['security'], 'ap_update_profile-' . $_POST['user']))
            die('Security check');

        $wpdb->insert(
            self::TABLE_NAME,
            [
                'user' => (int)$_POST['user'],
                'profile_name' => $_POST['profile_name'],
                'profile_type' => (int)$_POST['address_type'],
                'profile_fields' => '',
            ]
        );

        wp_die($wpdb->insert_id);
    }

    public function deleteProfileByType($type, $id) {
        global $wpdb;
        $wpdb->delete(self::TABLE_NAME, ['ID' => $id], ['%d']);
    }

    public function createProfileTable() {
        global $wpdb;

        $sql = "
          CREATE TABLE `address_profiles` (
          `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
          `user` INT(10) UNSIGNED NOT NULL,
          `profile_name` VARCHAR(45) NOT NULL DEFAULT '',
          `profile_type` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',
          `profile_fields` TEXT NOT NULL,
          PRIMARY KEY (`id`)
          ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
        ";

        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($sql);
    }
}