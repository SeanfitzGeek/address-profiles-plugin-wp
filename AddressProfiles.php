<?php
namespace AP\AddressProfiles;
use Model\AddressProfilesModal;

/*
Plugin Name: Address Profiles
Plugin URI:
Description: Allows saving and populating of multiple billing and shipping address profiles
Version:     1.0.0
Author:      Sean Fitzpatrick
Author URI:  https://bitbucket.org/SeanfitzGeek/
License:     GPL2
License URI: https://www.gnu.org/licenses/gpl-2.0.html
*/

class AddressProfiles {
    private static $instance = null;
    private $billingProfiles = [];

    /** @var AddressProfilesModal model */
    private $model = null;
    private $user = null;

    private $UPDATE_PATH = '';

    const VERSION = '1.0.0';
    const BILLING_PLUGIN_KEY = 'AP_Billing_Addresses';
    const SHIPPING_PLUGIN_KEY = 'AP_Shipping_Addresses';
    const BILLING_PROFILE = 0;
    const SHIPPING_PROFILE = 1;

    public static function instance() {
        if (!self::$instance)
            self::$instance = new static();

        return self::$instance;
    }

    public function run() {
        add_action('plugins_loaded', [$this, 'addPluginHooks']);
    }

    // Since pluggable isnt loaded until after
    public function addPluginHooks() {
        if (!($current_user = get_current_user_id()))
            return;

        require_once __DIR__ . '/model/Model.php';

        $this->model = new AddressProfilesModal();
        $this->UPDATE_PATH = plugin_dir_url(__FILE__) . '/update.php';
        $this->setUser($current_user);

        add_action('wp_enqueue_scripts', [$this, 'loadScripts'], 100);
        add_action('woocommerce_before_checkout_billing_form', [$this, 'cartAddBillingSelect'], 100);
        add_action('woocommerce_after_checkout_billing_form', [$this, 'cartAddBillingButton'], 100);
        add_action('wp_ajax_ap_update_profile', [$this->model, 'updateProfile']);
    }

    public function cartAddBillingSelect() {
        $return = '<div id="ap-address-profile-select" style="width: 100%; margin: 15px 0; float:left; clear:both;">
                    <div class="ap-address-load-fix"><label>Choose a saved Billing profile</label>
                    <select style="display:block;" name="billing-profiles">';

        $return .= '<option value="">----------</option>';

        foreach ($this->model->getProfilesByType(self::BILLING_PROFILE, $this->getUser()) as $profile)
            $return .= sprintf('<option value="%s">%s</option>', $profile->id, $profile->profile_name);

        $return .= '</select></div></div>';

        print($return);

        $this->cartAddNewBillingProfile();

        $this->addNewProfileModal();
    }

    // TODO move html into templates
    public function cartAddBillingButton() {
        printf(
            '<div style="clear: both;" data-user="%s" data-security="%s" data-address-type="billing" data-update-path="%s" >
                <a style="float:left; margin: 15px 0;" href="#" class="btn btn-primary profiles-save-billing-js">Save Address to Profile</a></div>',
            get_current_user_id(), wp_create_nonce('ap_update_profile-' . get_current_user_id()), $this->UPDATE_PATH);
    }

    public function cartAddNewBillingProfile() {
        print('<a href="#" id="new-billing-profile" class="btn btn-primary" style="float:left;" data-toggle="modal" data-target="#address-profile-modal">Add new Billing Profile</a>');
        print('<div style="clear:both"></div>');

    }

    public function cartAddShippingButton() {
        printf('<div data-update-path="%s" ><a style="float:left; margin: 15px 0;" href="#"
            class="btn btn-primary profiles-save-shipping-js">Save Address to Profile</a></div>', plugin_dir_url(__FILE__) . '/update.php');
    }

    public function addNewProfileModal() { ?>
        <div class="modal fade" tabindex="-1" role="dialog" id="address-profile-modal">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Enter new Billing Profile</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="new-billing-profile-input">New Billing Profile Name</label>
                            <input type="text" class="form-control" id="new-billing-profile-input" placeholder="Enter Profile Name">
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="js-add-profile btn btn-primary" data-security="<?= wp_create_nonce('ap_update_profile-' . $this->getUser()); ?>" data-user="<?= $this->getUser(); ?>">Add Profile</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
        <?php
    }

    public function loadScripts() {
        wp_register_script('address-profiles', plugin_dir_url(__FILE__) . 'js/profiles.js', ['jquery'], self::VERSION, true);
        wp_localize_script('address-profiles', 'ap_ajax_url', [admin_url('admin-ajax.php')]);
        wp_enqueue_script('address-profiles');
    }

    public function getBillingProfiles() {
        return $this->billingProfiles;
    }

    public function setBillingProfiles($profiles) {
        $this->billingProfiles = $profiles;

        return $this;
    }

    private function getUser() {
        return $this->user;
    }

    private function setUser($user) {
        if ($this->user !== null)
            return;

        $this->user = $user;

        return $this;
    }
}

//if (!class_exists('WooCommerce'))
AddressProfiles::instance()->run();